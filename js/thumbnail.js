import * as utils from "./utils"

/**
 * Determines the position at which we want to round off file size. 100 is 2 decimal places.
 *
 * @type {number}
 *
 * @const
 */
const ROUNDING_MULTIPLIER = 100;

/**
 * Variable used for calculating rounded size, in bytes, of dropped files.
 *
 * @type {number}
 *
 * @const
 */
const SIZE_BYTES = 1000;

/**
 * Variable  used for calculating rounded size, in Kilobytes, of dropped files.
 *
 * @type {number}
 *
 * @const
 */
const SIZE_KILOBYTES = SIZE_BYTES * 1000;

/**
 * Generates a Thumbnail object to manage the dropped / selected file on the DropZone, which is injected
 * in the DropZone.
 */
export default class Thumbnail {

    /**
     * Thumbnail Constructor
     *
     * @param {Object} args Arguments to setup the Thumbnail element.
     * @param {String} args.id - A unique to assign to this Object,m if provided.
     * @param {File} args.file_obj - The Dropped file to store with this object.
     * @param {Function} args.OnThumbnailRemoved - Callback function to be fired when the Thumbnail is removed from DropZone.
     *
     * @constructor
     */
    constructor(args) {

        /**
         * Variable to store ID provided as an argument.
         * @member {string}
         * @default {null}
         */
        this.id = args.id || null;

        /**
         * Variable to File being selected / dropped in the DropZone.
         * @member {File}
         * @default {null}
         */
        this.file_obj = args.file_obj || null;

        /**
         * Callback function for when the Thumbnail is removed from DropZone.
         * @member {Function}
         * @default {null}
         */
        this.OnThumbnailRemoved = args.OnThumbnailRemoved || null;

        /**
         * Variable to hold the Thumbnail DOM element.
         * @member {HTMLDivElement}
         * @default {null}
         */
        this.element = null;

        this.SetupDroppedElement();
    }

    /**
     *Sets up provided args and all the class variables.
     */
    SetupDroppedElement() {
        this.element = utils.CreateElement("div", this.id, "thumbnail");

        // Ensure that we stop the click event from getting propagated to DropZone
        this.element.addEventListener("click", (event) => {
            event.stopPropagation();
        }, false);

        // Add the elements to display file information
        this.AddElement("file_info truncate", this.file_obj.name);
        this.AddElement("file_info", this.GetFileSize(this.file_obj.size));

        // Add the remove button and its events
        this.AddRemoveButton();
    };

    /**
     * Wrapper function to create a div element, assign a CSS class , assign content, and add the resulting div to
     * the div element that this object maintains.
     *
     * @param {String} class_name - Class name to be assigned to the Div.
     * @param {HTMLElement | String} inner_html - An element or String to be added within the div.
     *
     * @returns {HTMLElement}
     */
    AddElement(class_name, inner_html) {
        let element = utils.CreateElement("div", "", class_name);
        element.innerHTML = inner_html;
        this.element.appendChild(element);
        return element;
    };

    /**
     * Return the size of the dropped file for display, either rounded to the nearest KB or MB.
     *
     * @param {number} size - Size in bytes.
     *
     * @returns {string} - Rounded size.
     */
    GetFileSize(size) {
        if(size < SIZE_BYTES) {
            return size + " bytes";
        }
        else if(size < SIZE_KILOBYTES) {
            return Math.round((size/SIZE_BYTES) * ROUNDING_MULTIPLIER) / ROUNDING_MULTIPLIER + " kb";
        }
        else {
            return Math.round((size/SIZE_KILOBYTES) * ROUNDING_MULTIPLIER) / ROUNDING_MULTIPLIER + " mb";
        }
    };

    /**
     * Adds a 'Remove' button to the Thumbnail.
     */
    AddRemoveButton() {
        let remove_button = this.AddElement("remove_button", "Remove");

        remove_button.addEventListener("click", (event) => {
            event.stopPropagation();
            this.OnThumbnailRemoved({
                File: this.file_obj,
                ThumbnailElement: this.element
            });
        }, false);
    };

    /**
     * @returns {File|null} - Returns the raw file or null.
     */
    GetFile() {
        return this.file_obj;
    };

    /**
     * @returns {HTMLElement|null} - Returns the Thumbnail element, or null, that the Thumbnail object has
     * generated for the dropped file.
     */
    GetElement() {
        return this.element;
    };
}