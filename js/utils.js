/**
 * Wrapper function for document.getElementById
 *
 * @param {String} id
 *
 * @returns {HTMLElement} found using the id.
 */
export let GetElementByID = (id) => {
    return document.getElementById(id);
};

/**
 * Wrapper function for document.createElement that adds the provided className and id to the
 * resulting element.
 *
 * @param {String} element_type
 * @param {String} id
 * @param {String} class_name
 *
 * @returns {HTMLElement} - The created element.
 */
export let  CreateElement = (element_type, id, class_name) => {
    let element = document.createElement(element_type);
    element.className = class_name;
    element.id = id;
    return element
};