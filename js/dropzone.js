import Thumbnail from "./thumbnail.js"
import * as utils from "./utils"

/**
 * Maximum number of file items to  be allowed
 *
 * @type {number}
 *
 * @const
 */
const MAX_ITEMS = 3;

/**
 * Used to create a new instance of DropZone.
 *
 * Variable to hold the DropZone Constructor returned from the self executing function.
 */
export default class Dropzone{

    /**
     * Processes the provided arguments and creates an instance of DropZone.
     *
     * @param {Object} args - Arguments to setup the DropZone element
     * @param {String} args.element - Name of the div element where the Dropzone needs attach to on a web page
     * @param {Number} args.maxFiles - Maximum number of files allowed on the DropZone at any given time.
     * @param {Function} args.OnAddedEvent - Callback function to be called when files are dropped or selected.
     * @param {Function} args.OnRemovedEvent - Callback function to be called when a file is removed from the DropZone.
     *
     * @constructor
     */
    constructor (args) {

        // If an element to attach to has not been provided then don't continue
        if(!args.element) {
            console.error("No element has been provided to attach the Drop Zone. Operation Aborted!");
            return;
        }

        /**
         * Variable to hold the DropZone Div Element.
         * @member {HTMLDivElement}
         * @default {null}
         */
        this.drop_zone = null;

        /**
         * Variable to hold the element which shows the default "Click or Drop Here" text.
         * @member {HTMLDivElement}
         * @default {null}
         */
        this.drop_zone_text = null;

        /**
         * Variable to hold hidden file selector.
         * @member {HTMLInputElement}
         * @default {null}
         */
        this.hidden_file_selector = null;

        /**
         * Variable to store the maximum number of files allowed at any given time..
         * @member {number}
         * @default {0}
         */
        this.max_files_allowed = 0;

        /**
         * Holds the list of Thumbnails currently in DropZone.
         * @member {Thumbnail[]}
         * @default {Array}
         */
        this.dropped_files_list = [];

        /**
         * Callback function for when a File is added to DropZone.
         * @member Function
         * @default {null}
         */
        this.OnFileAddFunction = args.OnAddedEvent || null;

        /**
         * Callback function for when a File is removed from DropZone.
         * @member Function
         * @default {null}
         */
        this.OnFileRemovedFunction = args.OnRemovedEvent || null;

        this.SetupArguments(args);
        this.SetupHiddenInput();
        this.SetupDropZone();
    }

    /**
    *   Sets up Dom elements and Class level variables from the provided arguments.
    *
    *   @param {Object} args - Arguments passed from constructor.
    */
    SetupArguments(args) {
        // Initialise the drop_zone at the provided element
        this.drop_zone = utils.GetElementByID(args.element);
        this.drop_zone.className = "drop_zone";

        // Setup any other provided arguments
        this.max_files_allowed = parseInt(args.maxFiles) || MAX_ITEMS;
    };

    /**
    *   Creates a hidden file selector input and binds it to the click event
    *   of the Drop Zone element.
    */
    SetupHiddenInput() {
        this.hidden_file_selector = utils.CreateElement("input", "", "");
        this.hidden_file_selector.setAttribute('type', 'file');
        this.hidden_file_selector.style.display = "none";
        this.hidden_file_selector.addEventListener('change', (event) => {
            this.OnDrop(event)
        }, false);

        this.hidden_file_selector.addEventListener('click', function(event){
            this.value = null;
        }, false);

        // Add the hidden element to the main drop_zone
        this.drop_zone.appendChild(this.hidden_file_selector);
    };

    /**
     *   Sets up the DropZone control. Describes and binds various functions to the
     *   click and drag/drop events for the element.
     */
    SetupDropZone() {
        this.drop_zone_text = utils.CreateElement("div", "", "drop_zone_text");
        this.drop_zone_text.innerHTML = "Click or Drop Here";
        this.drop_zone.appendChild(this.drop_zone_text);

        if(!window.File || !window.FileList || !window.FileReader) {
            this.drop_zone_text.innerHTML = "Browser not supported";
            return;
        }

        /**
         * Stops event bubbling up to parent event and prevents default action from happening.
         * @param event
         */
        let StopEventPropagation = (event) => {
            // Stop event propagation through the DOM
            event.stopPropagation();

            // Cancel the default action being taken by the browser on this event
            event.preventDefault();
        };

        /**
         * Sets the dropEffect to copy on when a file is dragged over the DropZone.
         */
        this.addEventListener("dragover", (event) => {
            event = event || window.event;
            StopEventPropagation(event);

            // Show this is a copy
            this.drop_zone.dropEffect = "copy";
        }, false);

        /**
         * Sets the DropZone ClassName to display the DragOver animation.
         */
        this.addEventListener("dragenter", () =>{
            this.drop_zone.className = "drop_zone drop_zone_on_drag";
        }, false);

        /**
         * Reset the DropZone ClassName once a file is dragged out of DropZone.
         */
        this.addEventListener("dragleave", () => {
            this.drop_zone.className = "drop_zone";
        }, false);

        /**
         * Reset the DropZone ClassName once a file is dropped into the DropZone and calls the OnDrop function.
         */
        this.addEventListener("drop", (event) => {
            event = event || window.event;
            StopEventPropagation(event);
            this.drop_zone.className = "drop_zone";

            this.OnDrop(event);
        }, false);

        /**
         * Displays File selector when the DropZone is clicked.
         */
        this.addEventListener("click", () => {
            // No need to process the event if we have reached max number of allowed items
            if (this.IsMaxedOut())
                return;

            let _event = document.createEvent("MouseEvents");
            _event.initEvent("click", false, false);
            this.hidden_file_selector.dispatchEvent(_event);
        }, false);
    };

    /**
     *   Handle the file drop event on the DropZone element.
     *
     *   @param {Event} event - which triggered this function.
     *   @param {target} event.target - Holds HTMLElement which fired the event This will be valid only if a file was
     *                                  selected using File Selector.
     *   @param {DataTransfer} event.dataTransfer - Holds data from Drag and Drop operation. This will be valid if a
     *                                  file has been dragged and dropped into the DropZone.
     */
    OnDrop(event) {
        // No need to process the event if we have reached max number of allowed items
        if (this.IsMaxedOut())
            return;

        // Get window.event if event argument if not provided
        event = event || window.event;

       /**
        * Get the files from either the File Input selector or the Files which have been directly dropped in.
        */
        let GetItems = () => {
            if("files" in event.target && event.target.files)
                return event.target.files;
            else if("files" in event.dataTransfer && event.dataTransfer.files)
                return event.dataTransfer.files;
        };

        // Store the file and display it on the DropZone
        this.AddItems(GetItems());
    };

    /**
     *   @return {boolean} - Whether the DropZone's maximum allowed file limit is reached.
     */
    IsMaxedOut() {
        // Check if the total files in the DropZone equals or exceeds the maximum allowed
        return this.drop_zone.querySelectorAll(".thumbnail").length >= this.max_files_allowed;
    };

    /**
     *   Creates new Thumbnail objects for the dropped / selected files, attaches required event handlers,
     *   and saves them on the global list.
     *
     *   @param {FileList} file_list - List of dropped files.
     */
    AddItems(file_list) {
        // Cache the length for faster looping further down the code
        let total_items = file_list.length;
        if(total_items > 0) {
            // Ensure that the text is hidden
            this.drop_zone_text.style.display = "none";

            /**
             * Callback function which will be added to each Thumbnail object. Ensure that the Thumbnail element is
             * removed from the DropZone Div Element.
             *
             * @type {function(this:Dropzone)}
             */
            let removed_callback = (detail) => {
                // Remove the Thumbnail object
                this.RemoveElement(detail);

                // Call the provided OnRemoved function if one is provided
                this.RaiseRemovedEvent(detail.File)
            };

            // Create Thumbnail objects to handle the dropped files.
            for(let i=0; i <= total_items - 1; i++) {
                let thumbnail = new Thumbnail({
                    id:                    "",
                    file_obj:              file_list[i],
                    OnThumbnailRemoved:    removed_callback
                });
                this.dropped_files_list.push(thumbnail);
                this.drop_zone.appendChild(thumbnail.GetElement());
            }

            // Call a function provided by the parent to notify about file added event
            this.RaiseAddedEvent(file_list);
        }
    };

    /**
     * Fires the callback function (OnFileAddFunction) whenever a file is added to DropZone.
     *
     * @param {FileList} added_files - Raw files which have been dropped / selected in the DropZone.
     */
    RaiseAddedEvent(added_files) {
        if(typeof (this.OnFileAddFunction) !== "function")
            return;

        // Ensure that a complete list of added files if provided
        this.OnFileAddFunction({AddedFiles: added_files});
    };

    /**
     * Fires the callback function (OnFileRemovedFunction) whenever a file is removed from DropZone.
     *
     * @param {File} file - Raw file which is being removed from the DropZone.
     */
    RaiseRemovedEvent(file) {
        if(typeof (this.OnFileRemovedFunction) !== "function")
            return;

        // Call the provided OnRemoved function if one is provided
        this.OnFileRemovedFunction({File: file});
    };

    /**
     * Removes the Thumbnail Div Element from DropZone.
     *
     * @param {Object} detail - Details of the file being removed.
     * @param {File} detail.File - The Raw File being removed.
     * @param {HTMLDivElement} detail.ThumbnailElement - The DOM element representing the Thumbnail in DropZone.
     */
    RemoveElement(detail) {
        // Remove the DOM element
        this.drop_zone.removeChild(detail.ThumbnailElement);

        // Remove the Thumbnail object
        let iLen = this.dropped_files_list.length;
        if(iLen === 1) {
            this.dropped_files_list.pop();
            this.drop_zone_text.style.display = "block";
            return;
        }

        for(let i=0; i<=iLen; i++) {
            if(this.dropped_files_list[i].GetElement() === detail.ThumbnailElement) {
                this.dropped_files_list.splice(i,1);
                break;
            }
        }
    };

    /**
     * Return a list of all files currently in the DropZone
     *
     * @returns {File[]} - List of files currently in the DropZone.
     */
    Files() {
        return this.dropped_files_list.map((thumbnail) => {
            return thumbnail.GetFile();
        });
    };

    /**
     * Wrapper function to attach an event handler to the DropZone (DOM) element.
     *
     * @param {String} event - The event to attach to.
     * @param {Function} _function - The callback function when event is fired.
     * @param {boolean} useCapture - Whether to set UseCapture.
     */
    addEventListener(event, _function, useCapture) {
        if(typeof (_function) !== "function")
            return;

        this.drop_zone.addEventListener(event, _function, useCapture);
    };
}