import Dropzone from "./dropzone";

/**
 * This function gets called when a file has been removed from the DropZone.
 *
 * @param {Object} detail - Dictionary of items being returned with this callback function is fired.
 * @param {File} detail.File - The raw file object object obtained after it was dropped / Selected in DropZone.
 */
let HandleFileRemovedEvent = (detail) => {
    console.log("File Removed: " + detail.File.name);
};

/**
 * This function gets called when a file has been selected or dropped in the DropZone.
 *
 * @param {Object} detail - Dictionary of items being returned with this callback function is fired.
 * @param {FileList} detail.AddedFiles - Total files that have been added to DropZone.
 */
let HandleFileAddedEvent = (detail) => {
    let file_list = detail.AddedFiles;
    let length = file_list.length;
    for(let i=0; i<length; i++) {
        console.log("New file added: " + file_list[i].name);
    }

    let files = DROP_ZONE.Files();
    let total_files = files.length;
    console.log("All files: ");
    for(let j=0; j<total_files; j++) {
        console.log((j+1).toString() + ": " + files[j].name);
    }
};

/**
 * Create a DropZone instance.
 */
let DROP_ZONE = new Dropzone({
    element:            "drop_zone",
    maxFiles:           5,
    OnAddedEvent:       HandleFileAddedEvent,
    OnRemovedEvent:     HandleFileRemovedEvent
});