# README #

### What is this repository for? ###

* Quick summary - DropZone is a simple JavaScript based control which allows a user to select or drop a file within a web page.

 
### Set up steps: ###

**Configurable options for a DropZone object:**

 * Parameter Name: **element**
    - Name of the div element where the DropZone needs to be placed on a web page must be provided.
 
 * Parameter Name: **max_files**
    - Maximum number concurrent files which should be allowed on the DropZone at any given time.
    Default is set to 3.
 
 * Parameter Name: **OnAddedEvent**
    - Callback function for when a file is dropped into DropZone.
    Default is set to null.
 
 * Parameter Name: **OnRemovedEvent**
    - Callback function for when a file is dropped into DropZone.
    Default is set to null.
  

**Compatible browsers**

* Chrome: 13+
* Firefox: 7+
* IE: 10+, Edge
* Opera: 12+
* Safari 6.0+

**Pre-requisites: **

* NodeJS - Get the latest from https://nodejs.org

**Deployment instructions** 


* Clone the repository and using either command prompt or teminal, navigate to the dropzone directory
* Run "npm install" command to install all the dependencies
* Run "npm run webpack" command to transpile ES6 code into ES5
* Run "npm start" to start the http server which will host the applicaiton.
* Please note that the application uses port 9000 by default, if this not available then the start command will throw an error. In this case, open up package.json and locate the scripts section. Modify the start settings to start the server on another port besides 9000.